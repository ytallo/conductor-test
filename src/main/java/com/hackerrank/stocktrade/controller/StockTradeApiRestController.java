package com.hackerrank.stocktrade.controller;

import com.hackerrank.stocktrade.model.Trade;
import com.hackerrank.stocktrade.service.TradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
public class StockTradeApiRestController {
    private final TradeService tradeService;

    @Autowired
    public StockTradeApiRestController(TradeService tradeService) {
        this.tradeService = tradeService;
    }

    @RequestMapping(path = "/trades", method = RequestMethod.POST, produces = { APPLICATION_JSON_VALUE }, consumes = { APPLICATION_JSON_VALUE })
    public ResponseEntity<Trade> createEndorsement(@RequestBody final Trade request) {
        Trade trade = tradeService.create(request);
        return ResponseEntity.ok(trade);
    }

    @RequestMapping(path = "/erase", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.OK)
    public void erase() {
        tradeService.deleteAll();
    }
}
