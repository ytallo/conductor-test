package com.hackerrank.stocktrade.service;

import com.hackerrank.stocktrade.model.Trade;
import com.hackerrank.stocktrade.repository.TradeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TradeService {

    private final TradeRepository repository;

    @Autowired
    public TradeService(TradeRepository repository) {
        this.repository = repository;
    }

    public Trade create(Trade entity) {
        return repository.save(entity);
    }

    public void deleteAll() {
        repository.deleteAll();
    }
}
